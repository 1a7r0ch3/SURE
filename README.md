# Stein's Unbiased Risk Estimators for [group norm denoising estimators](https://github.com/1a7r0ch3/group-norms)
Routines in GNU Octave or Matlab, with some C/C++ support with mex API.
Parallel implementation with OpenMP API.

## Available Routines

### SURE group norms denoising estimators with nonoverlapping group structures

SURE of the proximity operator of the ℓ<sub>1,2</sub> norm: `SURE_prox_l12.m`  
SURE of the proximity operator of the ℓ<sub>1,2</sub> bound (i.e. projection on the ℓ<sub>1,2</sub> ball): `SURE_proj_l12.m`  
SURE of the proximity operator of the reweighted ℓ<sub>1,2</sub> norm: `SURE_prox_rwl12.m` (requires mex `SURE_prox_rw12`)  
SURE of the proximity operator of the δ<sub>1,2</sub> semi-norm (used for *total variation* semi-norm): `SURE_prox_d12.m`  
SURE of the proximity operator of the δ<sub>1,2</sub> bound (i.e. projection on the δ<sub>1,2</sub> ball): `SURE_proj_l12.m`  
SURE of the proximity operator of the reweighted δ<sub>1,2</sub> semi-norm:
`SURE_prox_rwd12.m` (requires mex `SURE_prox_rw12`)  
SURE of the proximity operator of the δ<sub>1</sub> semi-norm over each edge of a graph (usefull for the *graph total variation*): mex `SURE_var_prox_graph_d1_mex`  

### Variances across several group norms denoising estimators; useful for taking into account overlapping group structures

Variance across proximity operators of ℓ<sub>1,2</sub> norms: mex `var_prox_l12.c`  
Variance across proximity operators of ℓ<sub>1,2</sub> bounds (i.e. projections on ℓ<sub>1,2</sub> balls): mex `var_proj_l12.c`  
Variance across proximity operators of reweighted ℓ<sub>1,2</sub> norm: mex `var_prox_rwl12`  
Variance across proximity operators of δ<sub>1,2</sub> semi-norms (used for *total variation* semi-norm): mex `var_prox_d12`  
Variance across proximity operators of δ<sub>1,2</sub> bounds (i.e. projections on δ<sub>1,2</sub> balls): mex `var_proj_d12`  
Variance across proximity operators of reweighted δ<sub>1,2</sub> semi-norms: mex `var_prox_rwd12`  
Variance across the proximity operators of the δ<sub>1</sub> semi-norm over each edge of a graph (usefull for the *graph total variation*): mex `SURE_var_prox_graph_d1_mex`  

## Directory tree and MEX routines
    mex/   
    ├── include/    C/C++ headers, with some doc  
    ├── doc/        some documentation  
    ├── api/        MEX API  
    └── src/        C/C++ sources  

Some MEX interfaces are documented within dedicated `.m` files in `mex/doc/`.  
Beware that currently, inputs are not checked, wrong input types and sizes lead to segmentation faults or aberrant results.  
See `mex/compile_mex.m` for typical compilation commands under UNIX systems.  

## Documentation
Octave or Matlab source files are documented in the header.
A small documentation for `SURE_var_prox_graph_d1_mex` can be found in directory mex/doc/.
See the [group norm denoising estimators](https://github.com/1a7r0ch3/group-norms) for a description of a group structure.
Usage of SURE and variances routines are exemplified by three functions written in GNU Octave or Matlab.  
`SURE_block_grids_2D.m` creates block structures and normalizations (requires routines from the [group-norms](https://github.com/1a7r0ch3/group-norms) repository) for block grids of different sizes over a two dimensional domain, and compute the SURE and variances across grids of proximity operators of desired group norms on the given signal.  
`best_estimators_set` selects the best best block structure and corresponding penalization scaling, based on the average of the SUREs and variances given by the above.  
`select_group_norm_param.m` reorganizes the block structures and penalizations for use with several independent signals

    [BlkStruct, penalizations, normalizations, SURE, Var] = ...
        SURE_block_grids_2D(signal, ROI, noise_estimate, szList, spList, 'prox_l12', ...
        0, inf, inf, verbose);
    [sIdx, ~, penalizations] = best_estimators_set(SURE, {}, penalizations);
    [BlkStruct, K, I, N, penalizations] = select_group_norm_param(BlkStruct, ...
        sIdx, penalizations, normalizations, true);

## References
H. Raguet, "A Signal Processing Approach to VSDOI", Chapter V, Ph.D. Thesis, 2014
