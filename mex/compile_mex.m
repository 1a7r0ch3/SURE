tmpDir = pwd; % remember working directory
cd(fileparts(which('compile_mex.m'))); 
mkdir('bin/');
try

if exist('OCTAVE_VERSION', 'builtin') % check if running Octave

    % compilation flags
    [~, CFLAGS] = system('mkoctfile -p CFLAGS');
    [~, CXXFLAGS] = system('mkoctfile -p CFLAGS');
    [~, LDFLAGS] = system('mkoctfile -p LDFLAGS');
    % some versions introduces a newline character (10)
    % in the output of 'system'; this must be removed
    if CFLAGS(end)==10, CFLAGS = CFLAGS(1:end-1); end
    if CXXFLAGS(end)==10, CXXFLAGS = CXXFLAGS(1:end-1); end
    if LDFLAGS(end)==10, LDFLAGS = LDFLAGS(1:end-1); end
    CFLAGS = sprintf('%s %s', CXXFLAGS, '-DMEX -fopenmp');
    CXXFLAGS = sprintf('%s %s', CXXFLAGS, '-DMEX -fopenmp');
    LDFLAGS = sprintf('%s %s', LDFLAGS, '-fopenmp');
    setenv('CFLAGS', CXXFLAGS);
    setenv('CXXFLAGS', CXXFLAGS);
    setenv('LDFLAGS', LDFLAGS);

    mex api/SURE_prox_rw12_mex.c src/SURE_prox_rw12.c -output bin/SURE_prox_rw12_mex

    mex api/var_prox_l12_mex.c src/var_prox_l12.c -output bin/var_prox_l12_mex

    mex api/var_prox_d12_mex.c src/var_prox_d12.c -output bin/var_prox_d12_mex

    mex api/var_proj_l12_mex.c src/var_proj_l12.c -output bin/var_proj_l12_mex

    mex api/var_proj_d12_mex.c src/var_proj_d12.c -output bin/var_proj_d12_mex

    mex api/var_prox_rwl12_mex.c src/var_prox_rwl12.c -output bin/var_prox_rwl12_mex

    mex api/var_prox_rwd12_mex.c src/var_prox_rwd12.c -output bin/var_prox_rwd12_mex

    mex api/SURE_var_prox_graph_d1_mex.cpp src/SURE_var_prox_graph_d1.cpp ...
        -output bin/SURE_var_prox_graph_d1_mex

else % running Matlab

    mex CFLAGS="\$CFLAGS -DMEX -fopenmp" LDFLAGS="\$LDFLAGS -fopenmp" ...
        api/SURE_prox_rw12_mex.c src/SURE_prox_rw12.c ...
        -output bin/SURE_prox_rw12_mex

    mex CFLAGS="\$CFLAGS -DMEX -fopenmp" LDFLAGS="\$LDFLAGS -fopenmp" ...
        api/var_prox_l12_mex.c src/var_prox_l12.c -output bin/var_prox_l12_mex

    mex CFLAGS="\$CFLAGS -DMEX -fopenmp" LDFLAGS="\$LDFLAGS -fopenmp" ...
        api/var_prox_d12_mex.c src/var_prox_d12.c -output bin/var_prox_d12_mex

    mex CFLAGS="\$CFLAGS -DMEX -fopenmp" LDFLAGS="\$LDFLAGS -fopenmp" ...
        api/var_proj_l12_mex.c src/var_proj_l12.c -output bin/var_proj_l12_mex

    mex CFLAGS="\$CFLAGS -DMEX -fopenmp" LDFLAGS="\$LDFLAGS -fopenmp" ...
        api/var_proj_d12_mex.c src/var_proj_d12.c -output bin/var_proj_d12_mex

    mex CFLAGS="\$CFLAGS -DMEX -fopenmp" LDFLAGS="\$LDFLAGS -fopenmp" ...
        api/var_prox_rwl12_mex.c src/var_prox_rwl12.c ...
        -output bin/var_prox_rwl12_mex

    mex CFLAGS="\$CFLAGS -DMEX -fopenmp" LDFLAGS="\$LDFLAGS -fopenmp" ...
        api/var_prox_rwd12_mex.c src/var_prox_rwd12.c -output bin/var_prox_rwd12_mex

    mex CXXFLAGS="\$CXXFLAGS -DMEX -fopenmp" LDFLAGS="\$LDFLAGS -fopenmp" ...
        api/SURE_var_prox_graph_d1_mex.cpp src/SURE_var_prox_graph_d1.cpp ...
        -output bin/SURE_var_prox_graph_d1_mex

end %endif running Octave

catch % if an error occur, makes sure not to change the working directory
	cd(tmpDir);
	rethrow(lasterror);
end
cd(tmpDir);
